CREATE OR REPLACE FUNCTION get_menu(r integer)
RETURNS SETOF menu LANGUAGE sql AS $$
  SELECT DISTINCT(m.*) FROM menu AS m
  INNER JOIN role ON role.id = any(string_to_array(m.role_id, ',')::int[])
  WHERE r = any(string_to_array(m.role_id, ',')::int[])
  ORDER BY m.order ASC;
$$;
