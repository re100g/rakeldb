truncate "specific_knowledge" cascade;
INSERT INTO public."specific_knowledge" (id, name) VALUES(1, 'Abas Erp');
INSERT INTO public."specific_knowledge" (id, name) VALUES(2, 'Acuamatica Cloud Erp');
INSERT INTO public."specific_knowledge" (id, name) VALUES(3, 'Adobe Acrobat Professional');
INSERT INTO public."specific_knowledge" (id, name) VALUES(4, 'Alegra');
INSERT INTO public."specific_knowledge" (id, name) VALUES(5, 'Allprowebtools');
INSERT INTO public."specific_knowledge" (id, name) VALUES(6, 'Alvendi');
INSERT INTO public."specific_knowledge" (id, name) VALUES(7, 'Anfix');
INSERT INTO public."specific_knowledge" (id, name) VALUES(8, 'Aquilon Erp');
INSERT INTO public."specific_knowledge" (id, name) VALUES(9, 'Automation Studio');
INSERT INTO public."specific_knowledge" (id, name) VALUES(10, 'Base');
INSERT INTO public."specific_knowledge" (id, name) VALUES(11, 'Bitrix24');
INSERT INTO public."specific_knowledge" (id, name) VALUES(12, 'Bizagi');
INSERT INTO public."specific_knowledge" (id, name) VALUES(13, 'Bizautomation');
INSERT INTO public."specific_knowledge" (id, name) VALUES(14, 'Cegit');
INSERT INTO public."specific_knowledge" (id, name) VALUES(15, 'Centralibos');
INSERT INTO public."specific_knowledge" (id, name) VALUES(16, 'Clauserp');
INSERT INTO public."specific_knowledge" (id, name) VALUES(17, 'Concar');
INSERT INTO public."specific_knowledge" (id, name) VALUES(18, 'Contalux');
INSERT INTO public."specific_knowledge" (id, name) VALUES(19, 'Contamoney');
INSERT INTO public."specific_knowledge" (id, name) VALUES(20, 'Contasimple');
INSERT INTO public."specific_knowledge" (id, name) VALUES(21, 'Contasol');
INSERT INTO public."specific_knowledge" (id, name) VALUES(22, 'Cronos');
INSERT INTO public."specific_knowledge" (id, name) VALUES(23, 'Data');
INSERT INTO public."specific_knowledge" (id, name) VALUES(24, 'Deacom');
INSERT INTO public."specific_knowledge" (id, name) VALUES(25, 'Delsol');
INSERT INTO public."specific_knowledge" (id, name) VALUES(26, 'Deltek');
INSERT INTO public."specific_knowledge" (id, name) VALUES(27, 'Deskera');
INSERT INTO public."specific_knowledge" (id, name) VALUES(28, 'Dolibarr Erp');
INSERT INTO public."specific_knowledge" (id, name) VALUES(29, 'Dropbox');
INSERT INTO public."specific_knowledge" (id, name) VALUES(30, 'Eci-M1');
INSERT INTO public."specific_knowledge" (id, name) VALUES(31, 'Ecount Erp');
INSERT INTO public."specific_knowledge" (id, name) VALUES(32, 'Epicor');
INSERT INTO public."specific_knowledge" (id, name) VALUES(33, 'Erpag');
INSERT INTO public."specific_knowledge" (id, name) VALUES(34, 'Erpnext');
INSERT INTO public."specific_knowledge" (id, name) VALUES(35, 'Evernote');
INSERT INTO public."specific_knowledge" (id, name) VALUES(36, 'E-Views');
INSERT INTO public."specific_knowledge" (id, name) VALUES(37, 'Factool');
INSERT INTO public."specific_knowledge" (id, name) VALUES(38, 'Factura Directa');
INSERT INTO public."specific_knowledge" (id, name) VALUES(39, 'Factusol');
INSERT INTO public."specific_knowledge" (id, name) VALUES(40, 'Financialforce Accounting');
INSERT INTO public."specific_knowledge" (id, name) VALUES(41, 'Forecast Pro Unlimited');
INSERT INTO public."specific_knowledge" (id, name) VALUES(42, 'Gespymes');
INSERT INTO public."specific_knowledge" (id, name) VALUES(43, 'Global Shop Solutions');
INSERT INTO public."specific_knowledge" (id, name) VALUES(44, 'Googledrive');
INSERT INTO public."specific_knowledge" (id, name) VALUES(45, 'Hightail');
INSERT INTO public."specific_knowledge" (id, name) VALUES(46, 'Hp Openview');
INSERT INTO public."specific_knowledge" (id, name) VALUES(47, 'Hubspot ');
INSERT INTO public."specific_knowledge" (id, name) VALUES(48, 'Idempiere');
INSERT INTO public."specific_knowledge" (id, name) VALUES(49, 'Infor Erp');
INSERT INTO public."specific_knowledge" (id, name) VALUES(50, 'Insightly');
INSERT INTO public."specific_knowledge" (id, name) VALUES(51, 'Iso 14001');
INSERT INTO public."specific_knowledge" (id, name) VALUES(52, 'Iso 18001');
INSERT INTO public."specific_knowledge" (id, name) VALUES(53, 'Iso 9001');
INSERT INTO public."specific_knowledge" (id, name) VALUES(54, 'Loggro');
INSERT INTO public."specific_knowledge" (id, name) VALUES(55, 'Logical Concept');
INSERT INTO public."specific_knowledge" (id, name) VALUES(56, 'Lotus Notes');
INSERT INTO public."specific_knowledge" (id, name) VALUES(57, 'Lotus Smart Suite');
INSERT INTO public."specific_knowledge" (id, name) VALUES(58, 'Macola');
INSERT INTO public."specific_knowledge" (id, name) VALUES(59, 'Made2Manage');
INSERT INTO public."specific_knowledge" (id, name) VALUES(60, 'Megainventory');
INSERT INTO public."specific_knowledge" (id, name) VALUES(61, 'Metlab');
INSERT INTO public."specific_knowledge" (id, name) VALUES(62, 'Microsoft Access');
INSERT INTO public."specific_knowledge" (id, name) VALUES(63, 'Microsoft Dynamics');
INSERT INTO public."specific_knowledge" (id, name) VALUES(64, 'Microsoft Excel');
INSERT INTO public."specific_knowledge" (id, name) VALUES(65, 'Microsoft Exchange');
INSERT INTO public."specific_knowledge" (id, name) VALUES(66, 'Microsoft Onedrive');
INSERT INTO public."specific_knowledge" (id, name) VALUES(67, 'Microsoft Onenote');
INSERT INTO public."specific_knowledge" (id, name) VALUES(68, 'Microsoft Powerpoint');
INSERT INTO public."specific_knowledge" (id, name) VALUES(69, 'Microsoft Project');
INSERT INTO public."specific_knowledge" (id, name) VALUES(70, 'Microsoft Visio');
INSERT INTO public."specific_knowledge" (id, name) VALUES(71, 'Microsoft Word');
INSERT INTO public."specific_knowledge" (id, name) VALUES(72, 'Microsoft Outlook');
INSERT INTO public."specific_knowledge" (id, name) VALUES(73, 'Mie Trak Pro');
INSERT INTO public."specific_knowledge" (id, name) VALUES(74, 'Mixerp');
INSERT INTO public."specific_knowledge" (id, name) VALUES(75, 'Moyex');
INSERT INTO public."specific_knowledge" (id, name) VALUES(76, 'Munis');
INSERT INTO public."specific_knowledge" (id, name) VALUES(77, 'Netsuite');
INSERT INTO public."specific_knowledge" (id, name) VALUES(78, 'Nomimasol');
INSERT INTO public."specific_knowledge" (id, name) VALUES(79, 'Odoo');
INSERT INTO public."specific_knowledge" (id, name) VALUES(80, 'Officebooks');
INSERT INTO public."specific_knowledge" (id, name) VALUES(81, 'Openbravo');
INSERT INTO public."specific_knowledge" (id, name) VALUES(82, 'Openpro Erp');
INSERT INTO public."specific_knowledge" (id, name) VALUES(83, 'Opmanager');
INSERT INTO public."specific_knowledge" (id, name) VALUES(84, 'Oracle Crm');
INSERT INTO public."specific_knowledge" (id, name) VALUES(85, 'Oracle E-Business Suite');
INSERT INTO public."specific_knowledge" (id, name) VALUES(86, 'Oracle Jd Edwards');
INSERT INTO public."specific_knowledge" (id, name) VALUES(87, 'Oracle Netsuite');
INSERT INTO public."specific_knowledge" (id, name) VALUES(88, 'Orcad');
INSERT INTO public."specific_knowledge" (id, name) VALUES(89, 'Pdf-Xchange Viewer');
INSERT INTO public."specific_knowledge" (id, name) VALUES(90, 'Peoplesoft');
INSERT INTO public."specific_knowledge" (id, name) VALUES(91, 'Pipedrive');
INSERT INTO public."specific_knowledge" (id, name) VALUES(92, 'Polygon Software');
INSERT INTO public."specific_knowledge" (id, name) VALUES(93, 'Prestacob');
INSERT INTO public."specific_knowledge" (id, name) VALUES(94, 'Process Simulator');
INSERT INTO public."specific_knowledge" (id, name) VALUES(95, 'Prophet21');
INSERT INTO public."specific_knowledge" (id, name) VALUES(96, 'Prosperworks');
INSERT INTO public."specific_knowledge" (id, name) VALUES(97, 'Sage Erp');
INSERT INTO public."specific_knowledge" (id, name) VALUES(98, 'Sage Intaact');
INSERT INTO public."specific_knowledge" (id, name) VALUES(99, 'Sage One');
INSERT INTO public."specific_knowledge" (id, name) VALUES(100, 'Sait Básico');
INSERT INTO public."specific_knowledge" (id, name) VALUES(101, 'Salesforce ');
INSERT INTO public."specific_knowledge" (id, name) VALUES(102, 'Salesnet');
INSERT INTO public."specific_knowledge" (id, name) VALUES(103, 'Sap');
INSERT INTO public."specific_knowledge" (id, name) VALUES(104, 'Sap Business One');
INSERT INTO public."specific_knowledge" (id, name) VALUES(105, 'Secomat');
INSERT INTO public."specific_knowledge" (id, name) VALUES(106, 'Secop');
INSERT INTO public."specific_knowledge" (id, name) VALUES(107, 'Sellsy');
INSERT INTO public."specific_knowledge" (id, name) VALUES(108, 'Siesa Erp');
INSERT INTO public."specific_knowledge" (id, name) VALUES(109, 'Siigo');
INSERT INTO public."specific_knowledge" (id, name) VALUES(110, 'Sispag');
INSERT INTO public."specific_knowledge" (id, name) VALUES(111, 'Skubana');
INSERT INTO public."specific_knowledge" (id, name) VALUES(112, 'Skype');
INSERT INTO public."specific_knowledge" (id, name) VALUES(113, 'Spss');
INSERT INTO public."specific_knowledge" (id, name) VALUES(114, 'Sugar');
INSERT INTO public."specific_knowledge" (id, name) VALUES(115, 'Syspro');
INSERT INTO public."specific_knowledge" (id, name) VALUES(116, 'Tca');
INSERT INTO public."specific_knowledge" (id, name) VALUES(117, 'Versaccounts');
INSERT INTO public."specific_knowledge" (id, name) VALUES(118, 'Vienna Advantage');
INSERT INTO public."specific_knowledge" (id, name) VALUES(119, 'Winfds');
INSERT INTO public."specific_knowledge" (id, name) VALUES(120, 'Winweb');
INSERT INTO public."specific_knowledge" (id, name) VALUES(121, 'Xtuple');
INSERT INTO public."specific_knowledge" (id, name) VALUES(122, 'Zendesk');
INSERT INTO public."specific_knowledge" (id, name) VALUES(123, 'Zoho Crm');
