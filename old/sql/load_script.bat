SET PGCLIENTENCODING=utf-8
psql -U hypatia -d quala -a -f age.sql
psql -U hypatia -d quala -a -f age_relation.sql
psql -U hypatia -d quala -a -f city_watson.sql
psql -U hypatia -d quala -a -f city_watson_relation.sql
psql -U hypatia -d quala -a -f gender.sql
psql -U hypatia -d quala -a -f gender_relation.sql
psql -U hypatia -d quala -a -f language.sql
psql -U hypatia -d quala -a -f language_relation.sql
psql -U hypatia -d quala -a -f position_category.sql
psql -U hypatia -d quala -a -f position.sql
psql -U hypatia -d quala -a -f position_relation.sql
psql -U hypatia -d quala -a -f specific_knowledge.sql
psql -U hypatia -d quala -a -f specific_knowledge_relation.sql
psql -U hypatia -d quala -a -f trade.sql
psql -U hypatia -d quala -a -f trade_relation.sql
