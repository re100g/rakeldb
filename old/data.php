<?php
	//http://localhost/queries/data.php?n=name&bc=1
	
	ini_set('max_execution_time', 600); 
	$name = $_GET['n'];
	$file = 'db/'.$name.'.csv';
	
	$table_n_1 = $name;
	$table_n_2 = $name.'_relation';

	$base_column = $_GET['bc'];
	$ids 		 = [];	
	$table_1     = [];
	$table_2 	 = [];
	$row 		 = 0;
	$row2 		 = 0;
	
	if (($gestor = fopen($file, 'r')) !== FALSE) {
    while (($data = fgetcsv($gestor, 10000, ';')) !== FALSE) {
		 	
			//if (isset($table_1[$data[$base_column]])) echo $data[$base_column].'<br>';
			if (isset($data[$base_column]) && !isset($table_1[$data[$base_column]])) {
      	$row++;
				$ids[$data[$base_column]] = $row;
				$table_1[$data[$base_column]] = 'INSERT INTO public."'.$table_n_1.'" (id, name) VALUES('.$row.', \''.$data[$base_column].'\');';
			}

      for ($c = ($base_column + 1); $c < sizeof($data); $c++) {
				$id = $ids[$data[$base_column]].'-'.$data[$c];

				if ($data[$c] && !isset($table_2[$id])) {
					$row2++;
					$table_2[$id] = 'INSERT INTO public."'.$table_n_2.'" (id, name, '.$name.'_id) VALUES('.$row2.', \''.$data[$c].'\', '.$ids[$data[$base_column]].');';
				}
      }
    }

  	fclose($gestor);
	}

	file_put_contents('dist/'.$table_n_1.'.sql', ('truncate "'.$table_n_1.'" cascade;').PHP_EOL, FILE_APPEND);
	foreach ($table_1 as $fields) {
		file_put_contents('dist/'.$table_n_1.'.sql', $fields.PHP_EOL, FILE_APPEND);
	}

	file_put_contents('dist/'.$table_n_2.'.sql', ('truncate "'.$table_n_2.'" cascade;').PHP_EOL, FILE_APPEND);
	foreach ($table_2 as $fields) {
		file_put_contents('dist/'.$table_n_2.'.sql', $fields.PHP_EOL, FILE_APPEND);
	}

	echo "done: ".$name;
