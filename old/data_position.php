<?php
	//http://localhost/queries/data_position.php
	
	ini_set('max_execution_time', 600); 
	$name = 'position';
	$file = 'db/'.$name.'.csv';
	
	$table_n_1 = $name;
	$table_n_2 = $name.'_relation';
	$table_n_0 = $name.'_category';

	$base_column = 1;
	$ids 		  = [];	
	$ids0     = [];
	$table_1  = [];
	$table_2 	= [];
	$row0 = 0;
	$row1 = 0;
	$row2 = 0;
	
	if (($gestor = fopen($file, 'r')) !== FALSE) {
    while (($data = fgetcsv($gestor, 10000, ';')) !== FALSE) {
		 	
			if (!isset($table_0[$data[$base_column - 1]])) {
				$row0++;
				$ids0[$data[$base_column - 1]]    = $row0;
				$table_0[$data[$base_column - 1]] = 'INSERT INTO public."'.$table_n_0.'" (id, name) VALUES('.$row0.', \''.$data[$base_column - 1].'\');';
			}
			
			if (isset($data[$base_column]) && !isset($table_1[$data[$base_column]])) {
      	$row1++;
				$ids[$data[$base_column]] = $row1;
				$table_1[$data[$base_column]] = 'INSERT INTO public."'.$table_n_1.'" (id, name, position_category_id) VALUES('.$row1.', \''.$data[$base_column].'\', '.$ids0[$data[$base_column -1]].');';
			}

      for ($c = ($base_column + 1); $c < sizeof($data); $c++) {
				$id = $ids[$data[$base_column]].'-'.$data[$c];

				if ($data[$c] && !isset($table_2[$id])) {
					$row2++;
					$table_2[$id] = 'INSERT INTO public."'.$table_n_2.'" (id, name, '.$name.'_id) VALUES('.$row2.', \''.$data[$c].'\', '.$ids[$data[$base_column]].');';
				}
      }
    }

  	fclose($gestor);
	}

	file_put_contents('dist/'.$table_n_1.'.sql', ('truncate "'.$table_n_1.'" cascade;').PHP_EOL, FILE_APPEND);
	foreach ($table_1 as $fields) {
		file_put_contents('dist/'.$table_n_1.'.sql', $fields.PHP_EOL, FILE_APPEND);
	}

	file_put_contents('dist/'.$table_n_2.'.sql', ('truncate "'.$table_n_2.'" cascade;').PHP_EOL, FILE_APPEND);
	foreach ($table_2 as $fields) {
		file_put_contents('dist/'.$table_n_2.'.sql', $fields.PHP_EOL, FILE_APPEND);
	}
	
	file_put_contents('dist/'.$table_n_0.'.sql', ('truncate "'.$table_n_0.'" cascade;').PHP_EOL, FILE_APPEND);
	foreach($table_0 as $fields) {
		file_put_contents('dist/'.$table_n_0.'.sql', $fields.PHP_EOL, FILE_APPEND);
	}

	echo "done";
