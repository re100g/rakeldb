<?php
	ini_set('max_execution_time', 600); 
	$name = $_GET['n'];
	$file = 'db/bases_v1/'.$name.'.csv';
	
	$table_n_1 = $name;
	$table_n_2 = $name.'_relation';

	$base_column = $_GET['bc'];
	$ids 		 = [];	
	$table_1 = [];
	$table_2 = [];
	$row 		 = 0;
	$row2 	 = 0;
	
	$et['Universitaria'] = 3;
	$et['Técnica o Tecnológica'] = 2;
	$et['Bachiller'] = 1;
	
	if (($gestor = fopen($file, 'r')) !== FALSE) {
    while (($data = fgetcsv($gestor, 10000, ';')) !== FALSE) {
			//if (isset($table_1[$data[$base_column]])) echo $data[$base_column].'<br>';
			if (isset($data[$base_column]) && !isset($table_1[$data[$base_column]])) {
      	$row++;
				$ids[$data[$base_column]] = $row;
				$table_1[$data[$base_column]] = $row.';"'.$data[$base_column].'"';
			
				if ($name == 'trade') {
					$table_1[$data[$base_column]] = $table_1[$data[$base_column]].';'.$et[$data[0]];
				}
			}

      for ($c = ($base_column + 1); $c < sizeof($data); $c++) {
				$id = $ids[$data[$base_column]].'-'.$data[$c];

				if ($data[$c] && !isset($table_2[$id])) {
					$row2++;
					$table_2[$id] = $row2.';"'.$data[$c].'";'.$ids[$data[$base_column]];
				} 
      }
    }

  	fclose($gestor);
	}
	
	if ($name == 'trade') {
		file_put_contents('dist/'.$table_n_1.'.csv', ('id;name;education_type_id').PHP_EOL, FILE_APPEND);
	} else {
		file_put_contents('dist/'.$table_n_1.'.csv', ('id;name').PHP_EOL, FILE_APPEND);
	}
	
	foreach ($table_1 as $fields) {
		file_put_contents('dist/'.$table_n_1.'.csv', $fields.PHP_EOL, FILE_APPEND);
	}

	file_put_contents('dist/'.$table_n_2.'.csv', ('id;name;'.$name.'_id').PHP_EOL, FILE_APPEND);
	foreach ($table_2 as $fields) {
		file_put_contents('dist/'.$table_n_2.'.csv', $fields.PHP_EOL, FILE_APPEND);
	}
	
	echo "done: ".$name;

	/*
	n = nombre del csv sin extension bc = base column donde esta la primera columna
	http://localhost/rakeldb/data.php?n=age&bc=0
	http://localhost/rakeldb/data.php?n=city_watson&bc=0
	http://localhost/rakeldb/data.php?n=gender&bc=0
	http://localhost/rakeldb/data.php?n=language&bc=0
	http://localhost/rakeldb/data.php?n=specific_knowledge&bc=0
	http://localhost/rakeldb/data.php?n=trade&bc=1
	http://localhost/rakeldb/data_position.php?n=position&bc=0
	*/