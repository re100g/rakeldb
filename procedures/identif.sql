create or replace function  Identest () returns integer

as

$$

declare


identificador record;

cur_identest Cursor for select * from st_test;
	

begin

Open cur_identest;
loop

Fetch cur_identest into identificador;

IF NOT FOUND THEN
            EXIT;
			END IF;
			
			update test_question_answer set st_test_id = identificador.id where user_id = identificador.user_id and process_selection_id = identificador.process_selection_id;
			
			

end loop;
    
CLOSE cur_identest;

return 1;

end $$

language 'plpgsql';
