create function dropout(correo character varying) returns integer
    language plpgsql
as
$$
declare


BEGIN
--creamos el cursor para reemplazar el correo

delete from cv_file where curriculum_vitae_id in
(select cv.id from curriculum_vitae as cv
inner join "user" as u on cv.user_id=u.id
where email=correo);

delete from cv_higher_education where curriculum_vitae_id in (select cv.id from curriculum_vitae as cv
inner join "user" as u on u.id=cv.user_id
where email=correo);

delete from cv_language where curriculum_vitae_id in (select cv.id from curriculum_vitae as cv
inner join "user" as u on u.id=cv.user_id
where email=correo) ;

delete from cv_process_selection where curriculum_vitae_id in
(select cv.id from curriculum_vitae as cv
inner join "user" as u on cv.user_id=u.id
where email=correo);

delete from cv_specific_knowledge where curriculum_vitae_id in (select cv.id from curriculum_vitae as cv
inner join "user" as u on u.id=cv.user_id
where email=correo);

delete from cv_work_experience where curriculum_vitae_id in (select cv.id from curriculum_vitae as cv
inner join "user" as u on u.id=cv.user_id
where email=correo);

delete from curriculum_vitae where user_id in
(select id from "user" where email=correo);

delete from log_assistan_counter where user_id in
(select id from "user" where email=correo);

delete from session  where user_id in
(select id from "user" where email=correo);

delete from st_consolidated_result_certification where st_consolidated_result_id in
(select cr.id from st_consolidated_result as cr
inner join "user" as u on cr.user_id = u.id where email=correo);

delete from st_consolidated_result_experience where st_consolidated_result_id in
(select cr.id from st_consolidated_result as cr
inner join "user" as u on cr.user_id = u.id where email=correo);

delete from st_consolidated_result_language where st_consolidated_result_id in
(select cr.id from st_consolidated_result as cr
inner join "user" as u on cr.user_id = u.id where email=correo);

delete from st_consolidated_result_professional_license where st_consolidated_result_id in
(select cr.id from st_consolidated_result as cr
inner join "user" as u on cr.user_id = u.id where email=correo);

delete from st_consolidated_result_specific_knowledge where st_consolidated_result_id in
(select cr.id from st_consolidated_result as cr
inner join "user" as u on cr.user_id = u.id where email=correo);

delete from st_consolidated_result_style_feature where st_consolidated_result_id in
(select cr.id from st_consolidated_result as cr
inner join "user" as u on cr.user_id = u.id where email=correo);

delete from st_consolidated_result where user_id in
(select id from "user" where email=correo);

delete from st_depth_interview_results where st_depth_interview_id in
(select st_depth_interview_id from st_depth_interview_results as dir
inner join st_depth_interview as di on di.id=dir.st_depth_interview_id
where di.user_id =(select id from "user" where email=correo));

delete from st_depth_interview_user_answer where st_depth_interview_id in
(select st_depth_interview_id from st_depth_interview_user_answer as diu
inner join st_depth_interview as di on di.id=diu.st_depth_interview_id
where di.user_id =(select id from "user" where email=correo));

delete from st_depth_interview
where user_id in (select id from "user" where email=correo);

delete from st_exploratory_interview_user_answer where id in
(select eia.id from st_exploratory_interview_user_answer as eia
inner join st_exploratory_interview as ei on eia.st_exploratory_interview_id=ei.id
where ei.user_id in (select id from "user" where email=correo));

delete from st_exploratory_interview where user_id in
(select id from "user" where email=correo);

delete from st_final_selected_user where user_id in
(select id from "user" where email=correo);

delete from test_question_answer where user_id in
(select tqa.user_id from test_question_answer as tqa
inner join "user" as u on u.id=tqa.user_id where u.id in
(select id from "user" where email=correo));

delete from test_result where user_id in
(select id from "user" where email=correo);

delete from st_test where user_id in (select id from "user" where email=correo);

delete from "user" where email=correo;

return 1;

END
$$;

alter function dropout(varchar) owner to postgres;

-- para la ultima version de la db

create function dropout(correo character varying) returns integer
    language plpgsql
as
$$
declare


BEGIN
--creamos el cursor para reemplazar el correo

delete from cv_file where curriculum_vitae_id in
(select cv.id from curriculum_vitae as cv
inner join "user" as u on cv.user_id=u.id
where email=correo);

delete from cv_higher_education where curriculum_vitae_id in (select cv.id from curriculum_vitae as cv
inner join "user" as u on u.id=cv.user_id
where email=correo);

delete from cv_language where curriculum_vitae_id in (select cv.id from curriculum_vitae as cv
inner join "user" as u on u.id=cv.user_id
where email=correo) ;

delete from cv_process_selection where curriculum_vitae_id in
(select cv.id from curriculum_vitae as cv
inner join "user" as u on cv.user_id=u.id
where email=correo);

delete from cv_specific_knowledge where curriculum_vitae_id in (select cv.id from curriculum_vitae as cv
inner join "user" as u on u.id=cv.user_id
where email=correo);

delete from cv_work_experience where curriculum_vitae_id in (select cv.id from curriculum_vitae as cv
inner join "user" as u on u.id=cv.user_id
where email=correo);

delete from curriculum_vitae where user_id in
(select id from "user" where email=correo);

delete from exploratory_interview_report where user_id in
(select id from "user" where email=correo);

delete from log_assistant_counter where user_id in
(select id from "user" where email=correo);

delete from session  where user_id in
(select id from "user" where email=correo);

delete from st_consolidated_result_certification where st_consolidated_result_id in
(select cr.id from st_consolidated_result as cr
inner join "user" as u on cr.user_id = u.id where email=correo);

delete from st_consolidated_result_experience where st_consolidated_result_id in
(select cr.id from st_consolidated_result as cr
inner join "user" as u on cr.user_id = u.id where email=correo);

delete from st_consolidated_result_language where st_consolidated_result_id in
(select cr.id from st_consolidated_result as cr
inner join "user" as u on cr.user_id = u.id where email=correo);

delete from st_consolidated_result_professional_license where st_consolidated_result_id in
(select cr.id from st_consolidated_result as cr
inner join "user" as u on cr.user_id = u.id where email=correo);

delete from st_consolidated_result_specific_knowledge where st_consolidated_result_id in
(select cr.id from st_consolidated_result as cr
inner join "user" as u on cr.user_id = u.id where email=correo);

delete from st_consolidated_result_style_feature where st_consolidated_result_id in
(select cr.id from st_consolidated_result as cr
inner join "user" as u on cr.user_id = u.id where email=correo);

delete from st_consolidated_result where user_id in
(select id from "user" where email=correo);

delete from st_depth_interview_results where st_depth_interview_id in
(select st_depth_interview_id from st_depth_interview_results as dir
inner join st_depth_interview as di on di.id=dir.st_depth_interview_id
where di.user_id =(select id from "user" where email=correo));

delete from st_depth_interview_user_answer where st_depth_interview_id in
(select st_depth_interview_id from st_depth_interview_user_answer as diu
inner join st_depth_interview as di on di.id=diu.st_depth_interview_id
where di.user_id =(select id from "user" where email=correo));

delete from st_depth_interview
where user_id in (select id from "user" where email=correo);

delete from st_exploratory_interview_user_answer where id in
(select eia.id from st_exploratory_interview_user_answer as eia
inner join st_exploratory_interview as ei on eia.st_exploratory_interview_id=ei.id
where ei.user_id in (select id from "user" where email=correo));

delete from st_exploratory_interview where user_id in
(select id from "user" where email=correo);

delete from st_final_selected_user_comment where id in
(select stfsuc.id from st_final_selected_user_comment as stfsuc
inner join st_final_selected_user as stfsu on stfsuc.st_final_selected_user_id=stfsu.id
where stfsu.user_id in (select id from "user" where email=correo));

delete from st_final_selected_user where user_id in
(select id from "user" where email=correo);

delete from test_question_answer where user_id in
(select tqa.user_id from test_question_answer as tqa
inner join "user" as u on u.id=tqa.user_id where u.id in
(select id from "user" where email=correo));

delete from test_result where user_id in
(select id from "user" where email=correo);

delete from st_test where user_id in (select id from "user" where email=correo);

delete from "user" where email=correo;

return 1;

END
$$;

alter function dropout(varchar) owner to postgres;
