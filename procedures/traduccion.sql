create or replace function  Traduccion (usuario bigint, proceso bigint)
returns table(id_def varchar, name_def varchar)

AS $$
declare
    registro varchar (500);
    traduccion Record;
    cur_traduccion Cursor for select * from st_exploratory_interview  as eia
    inner join st_exploratory_interview_user_answer  ei on eia.id=ei.st_exploratory_interview_id
    where user_id = usuario and process_selection_id = proceso;

begin
create temp table definition(configuration_id varchar, valor varchar);
Open cur_traduccion;
loop
Fetch cur_traduccion into traduccion;
IF NOT FOUND THEN
            EXIT;
			END IF;
CASE traduccion.model
		when 'City' then
        select name
		into registro
		from city_watson where id = traduccion.value;
        when 'Language' then
        select name
		into registro
		from language where id = traduccion.value;
        when 'Experience' then
        select name
		into registro
		from experience where id = traduccion.value;
        when 'Certification' then
        select name
		into registro
		from certification where id = traduccion.value;
        when 'Age' then
        select name
		into registro
		from age where id = traduccion.value;
        when 'Trade' then
        select name
		into registro
		from trade where id = traduccion.value;
        when 'ExploratoryInterviewAnswer' then
        select name
		into registro
		from exploratory_interview_answer where id = traduccion.value;
        when 'SalaryRange' then
		select low_range || '-' || high_range
		into registro
		from salary_range where id = traduccion.value;
        else
 Raise notice 'Model:% , value:% , answer:%', traduccion.Model, traduccion.value, traduccion.answer;
    end case;


insert into definition values (traduccion.configuration_id, registro);
UPDATE definition SET valor = 'No_presenta' WHERE valor IS null;
END LOOP ;
CLOSE cur_traduccion;

  RETURN QUERY SELECT
    *
  FROM
     definition;
end $$
language 'plpgsql';create or replace function  Traduccion (usuario bigint, proceso bigint)
returns table(id_def varchar, name_def varchar)

AS $$
declare
    registro varchar (500);
    traduccion Record;
    cur_traduccion Cursor for select * from st_exploratory_interview  as eia
    inner join st_exploratory_interview_user_answer  ei on eia.id=ei.st_exploratory_interview_id
    where user_id = usuario and process_selection_id = proceso;

begin
create temp table definition(configuration_id varchar, valor varchar);
Open cur_traduccion;
loop
Fetch cur_traduccion into traduccion;
IF NOT FOUND THEN
            EXIT;
			END IF;
CASE traduccion.model
		when 'City' then
        select name
		into registro
		from city_watson where id = traduccion.value;
        when 'Language' then
        select name
		into registro
		from language where id = traduccion.value;
        when 'Experience' then
        select name
		into registro
		from experience where id = traduccion.value;
        when 'Position' then
        select name
		into registro
		from Position where id = traduccion.value;
        when 'Certification' then
        select name
		into registro
		from certification where id = traduccion.value;
        when 'Age' then
        select name
		into registro
		from age where id = traduccion.value;
        when 'Trade' then
        select name
		into registro
		from trade where id = traduccion.value;
        when 'ExploratoryInterviewAnswer' then
        select name
		into registro
		from exploratory_interview_answer where id = traduccion.value;
        when 'SalaryRange' then
		select low_range || '-' || high_range
		into registro
		from salary_range where id = traduccion.value;
        else
 Raise notice 'Model:% , value:% , answer:%', traduccion.Model, traduccion.value, traduccion.answer;
    end case;


insert into definition values (traduccion.configuration_id, registro);
UPDATE definition SET valor = 'No_presenta' WHERE valor IS null;
END LOOP ;
CLOSE cur_traduccion;

  RETURN QUERY SELECT
    *
  FROM
     definition;
end $$
language 'plpgsql';