<?php
	ini_set('max_execution_time', 600); 
	$name = 'position';
	$file = 'db/bases_v1/'.$name.'.csv';
	
	$table_n_0 = $name.'_level';
	$table_n_1 = $name.'_category';
	$table_n_2 = $name;
	$table_n_3 = $name.'_relation';

	$base_column = 2;
	$ids0 = [];
	$ids1 = [];
	$ids2 = [];	
	$table_0 = [];
	$table_1 = [];
	$table_2 = [];
	$table_3 = [];
	$row0 = 0;
	$row1 = 0;
	$row2 = 0;
	$row3 = 0;
	
	if (($gestor = fopen($file, 'r')) !== FALSE) {
		while (($data = fgetcsv($gestor, 10000, ';')) !== FALSE) {
				
			// Record to position level
			if (!isset($table_0[trim($data[$base_column - 2])])) {
				$name = trim($data[$base_column - 2]);
				if ($name == 'Operativo') $row0 = 1;
				if ($name == 'Asistencial') $row0 = 2;
				if ($name == 'Profesional') $row0 = 3;
				$ids0[$data[$base_column - 2]]    = $row0;
				$table_0[$data[$base_column - 2]] = $row0.';"'.$name.'"';
			}
				
			// Record for position category
			if (!isset($table_1[$data[$base_column - 1]])) {
				$row1++;
				$ids1[$data[$base_column - 1]]    = $row1;
				$table_1[$data[$base_column - 1]] = $row1.';"'.$data[$base_column - 1].'"';
			}
			
			// Record for position
			if (isset($data[$base_column]) && !isset($table_2[$data[$base_column]])) {
				$row2++;
				$ids2[$data[$base_column]] = $row2;
				$table_2[$data[$base_column]] = $row2.';"'.$data[$base_column].'";'.$ids1[$data[$base_column - 1]].';'.$ids0[trim($data[$base_column - 2])];
			}

			// Record for position relation
			for ($c = ($base_column + 1); $c < sizeof($data); $c++) {
				$id = $ids2[$data[$base_column]].'-'.$data[$c];

				if ($data[$c] && !isset($table_3[$id])) {
					$row3++;
					$table_3[$id] = $row3.';"'.$data[$c].'";'.$ids2[$data[$base_column]];
				}
			}
		}

		fclose($gestor);
	}

	// Save to position level
	file_put_contents('dist/'.$table_n_0.'.csv', ('id;name').PHP_EOL, FILE_APPEND);
	foreach ($table_0 as $fields) {
		file_put_contents('dist/'.$table_n_0.'.csv', $fields.PHP_EOL, FILE_APPEND);
	}
	
	// Save to position category
	file_put_contents('dist/'.$table_n_1.'.csv', ('id;name').PHP_EOL, FILE_APPEND);
	foreach ($table_1 as $fields) {
		file_put_contents('dist/'.$table_n_1.'.csv', $fields.PHP_EOL, FILE_APPEND);
	}
	
	// Save to position
	file_put_contents('dist/'.$table_n_2.'.csv', ('id;name;position_category_id;position_level_id').PHP_EOL, FILE_APPEND);
	foreach ($table_2 as $fields) {
		file_put_contents('dist/'.$table_n_2.'.csv', $fields.PHP_EOL, FILE_APPEND);
	}
	
	// Save to position relation
	file_put_contents('dist/'.$table_n_3.'.csv', ('id;name;position_id').PHP_EOL, FILE_APPEND);
	foreach ($table_3 as $fields) {
		file_put_contents('dist/'.$table_n_3.'.csv', $fields.PHP_EOL, FILE_APPEND);
	}
	
	echo "done";
